
function getLoan() {
    let bankBalance = (document.getElementById("balance").innerHTML).slice(0, -2);
    let existing_loan = document.getElementById("loan").innerHTML;
    let amount = prompt("Please enter the desired loan amount: ");
    if (existing_loan != "0kr") {
        alert("You have to repay your existing loan before getting another loan.")
    }else{
        if (amount > 2*bankBalance) {
            alert("You are cannot loan more than double your back balance.")
        }else{
            document.getElementById("loan").innerHTML = amount + "kr";
            document.getElementById("btnRL").style.display = "block";
        }
    }
}

function repayLoan() {
    let existing_loan = parseInt(document.getElementById("loan").innerHTML.slice(0,-2), 10);
    let pay = parseInt(document.getElementById("pay").innerHTML.slice(0,-2), 10);
    if (pay > existing_loan) {
        document.getElementById("loan").innerHTML = "0kr";
        document.getElementById("btnRL").style.display = "none";
        let toBank = pay-existing_loan
        let balance = parseInt(document.getElementById("balance").innerHTML.slice(0,-2), 10);
        document.getElementById("balance").innerHTML = (toBank + balance) + "kr";
    }else{
        document.getElementById("loan").innerHTML = existing_loan-pay+"kr";
    }
    document.getElementById("pay").innerHTML = 0 + "kr";
}

function work() {
    let value = parseInt((document.getElementById("pay").innerHTML).slice(0, -2), 10);
    document.getElementById("pay").innerHTML = (value+100)+"kr";
}

function bank() {
    let payStr = document.getElementById("pay").innerHTML;
    let balanceStr = document.getElementById("balance").innerHTML;
    let loan = parseInt(document.getElementById("loan").innerHTML, 10);
    let fullValue = parseInt(payStr.slice(0,-2), 10) + parseInt(balanceStr.slice(0,-2), 10);
    let tenPay = (parseInt(payStr.slice(0,-2), 10)*0.1);
    let ninetyPayAndBalance = (parseInt(payStr.slice(0,-2), 10)*0.9) + parseInt(balanceStr.slice(0,-2), 10);
    if (loan > 0) {
        let restLoan = loan-tenPay;
        if (restLoan <= 0){
            document.getElementById("loan").innerHTML = 0+"kr";
        } else {
            document.getElementById("loan").innerHTML = restLoan+"kr";
        }
        document.getElementById("balance").innerHTML = ninetyPayAndBalance + "kr";
    } else {
        document.getElementById("balance").innerHTML = fullValue + "kr";
    }
    document.getElementById("pay").innerHTML = 0 + "kr";
}
let fruits = [];
async function fetchy() {
    await fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
  .then((response) => response.json())
  .then((data) => data.forEach(element => {
    fruits.push(element)
  }));
  select = document.getElementById("lappytoppy");
  for (let i = 0; i < fruits.length; i++) {
    let option = document.createElement("option");
        option.value = fruits[i].description;
        option.text = fruits[i].title;
        option.id = fruits[i].id;
        console.log()
        option.name = fruits[i].id;
        select.appendChild(option);
  }
}

function displayLaptop() {
    let ailoGaup = document.getElementById("lappytoppy");
    let element = fruits[ailoGaup.selectedIndex-1];
    document.getElementById("title").innerHTML = element.title;
    document.getElementById("description").innerHTML = element.description;
    document.getElementById("price").innerHTML = element.price;
    let specz = document.getElementById("specs");
    specz.innerHTML = "";
    for (let i = 0; i < element.specs.length; i++) {
        let li = document.createElement("li");
        li.innerHTML = element.specs[i]
        specz.appendChild(li)
    }
    let id = element.id;
    document.getElementById("imigy").src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/"+id+".png"
}

function buyLaptop() {
    let balance = parseInt(document.getElementById("balance").innerHTML.slice(0,-2), 10);
    let price = document.getElementById("price").innerHTML;
    console.log(price)
    console.log("b: "+balance)
    if (price > balance) {
        alert("Sorry sir, you unfortunately do not have enough money to purchase this komputer :(")
    } else {
        document.getElementById("balance").innerHTML = (balance - price) +"kr";
        alert("You are now the owner of a new laptop! :D")
    }
}